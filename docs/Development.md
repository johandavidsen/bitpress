# Development

## 2015-09-14

Prototyping: Executinng the following command gives all the repos publically
available.

    curl https://api.bitbucket.org/2.0/repositories/johandavidsen

The next step is then to iterate through the different repositories an access the list
of commits.

From the return string, I need to access the commits field of the JSON object.
See example:

    curl https://api.bitbucket.org/2.0/repositories/johandavidsen/bitpress/commits

From the return string, I need to iterate through the objects and sort them by
date. This procedure needs to be done for all the repositories. The commits need
to be sorted by date and filtered by author.

This approach only works for public repositories.
