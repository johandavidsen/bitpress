<?php

class Commit {

    public $author;

    public $date;

    public $type;

    public $message;

    public static function fetch_commits($repo){

        $commits = array();

        $json = WebServiceCalls::get($repo);

        if(!is_object($json = json_decode($json)))
            return FALSE;

        foreach( $json -> values as $commit) {

            $com = new Commit;
            $com->author = $commit -> author;

            $commits[] = $com;
        }

        return $commits;
    }
}

?>
